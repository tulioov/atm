package atm.account;

import atm.account.model.Account;

/**
 * Account Service
 *
 * @author Fittipaldi, Roberval, Lucas, Túlio
 */
public class ConsultaAccountService extends AccountService {

	@Override
	public boolean validarUsuario(Account conta) {
		
		if((conta.getAccountNumber() == 123) && (conta.getPin() == 321)) {
			return true;
		}
		return false;
	}

	@Override
	public Double consultarSaldo(Account conta) {
		log.add("Consultando saldo de R$ " + conta.getSaldo());
		return conta.getSaldo();
	}
}
