package atm.account;

import atm.Log;
import atm.account.model.Account;

/**
 * Account Service
 *
 * @author Fittipaldi, Roberval, Lucas, Túlio
 */
public class AccountService {
	
	protected Log log = Log.getInstance();
	
	/**
	 * Depositar
	 * @param conta {@link Account}
	 * @param valor {@link Double}
	 */
	public void depositar(Account conta, String valor) {}
	
	/**
	 * Efetuar Saque
	 * @param conta {@link Account}
	 * @param valor {@link Double}
	 * @return True ou False
	 */	
	public boolean efetuarSaque(Account conta, Double valor) {
		return false;
	};

	/**
	 * Valida Usuário
	 * @param conta {@link Account}
	 * @return True ou False
	 */
	public boolean validarUsuario(Account conta) {
		return false;
	}

	/**
	 * Consulta Saldo
	 * @param conta {@link Account}
	 * @return True ou False
	 */
	public Double consultarSaldo(Account conta) {
		return null;
	}
}
