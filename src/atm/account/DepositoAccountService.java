package atm.account;

import atm.account.model.Account;

public class DepositoAccountService extends AccountService {

	@Override
	public void depositar(Account conta, String valor) {
		log.add("Depositando um valdor de R$ " + valor);
		conta.setSaldo(conta.getSaldo() + Double.parseDouble(valor));
	}
}
