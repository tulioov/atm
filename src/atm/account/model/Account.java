package atm.account.model;

import java.io.Serializable;

/**
 * Classe Modelo Account 
 * 
 * @author Fittipaldi, Roberval, Túlio, Lucas
 */
public class Account implements Serializable {
	
	private static final long serialVersionUID = -3835350393778403266L;
	
	private int    accountNumber; 
	private int    pin; 
	private double saldo;
	
	/**
	 * Construtor
	 * @param accountNumber Número da Conta
	 * @param pin PIN
	 * @param saldo Saldo
	 */
	public Account(int accountNumber, int pin, double saldo) {
		this.accountNumber = accountNumber;
		this.pin           = pin;
		this.saldo         = saldo;
	}

	/**
	 * Construtor Padrão
	 */
	public Account() {}

	/**
	 * Valida PIN
	 * @param userPIN PIN User
	 * @return True or False
	 */
	public boolean validatePIN(int userPIN) {
		if (userPIN == pin) {
			return true; 
		} else {
			return false; 
		}
	} 

	public int getAccountNumber() {
		return accountNumber;
	}

	public int getPin() {
		return pin;
	}

	public void setPin(int pin) {
		this.pin = pin;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
} 
