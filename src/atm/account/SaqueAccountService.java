package atm.account;

import atm.account.model.Account;

public class SaqueAccountService extends AccountService {

	@Override
	public boolean efetuarSaque(Account conta, Double valor) {
		log.add("Efetuando Saque de R$ "  + valor);
		if(conta.getSaldo()<valor) {
			return false;
		}else {
			conta.setSaldo(conta.getSaldo()-valor);
			return true;
		}
	}
	
}
