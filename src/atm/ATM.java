
package atm;

/**
 * ATM Simulador Bank
 * 
 * @author Fittipaldi, Roberval, Túlio, Lucas
 */
public class ATM {

	/**
	 * Método Principal para do ATM
	 * @param args Argumentos
	 */
    public static void main(String[] args) {
        
        java.awt.EventQueue.invokeLater(() -> {
            new ATMView().setVisible(true);
        });
        
    }
    
}
