
package atm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import atm.account.AccountService;
import atm.account.ConsultaAccountService;
import atm.account.DepositoAccountService;
import atm.account.SaqueAccountService;
import atm.account.model.Account;

/**
 * Controlador do ATM
 */
public class ATMController {

	private ATMView        v;
	private Account        conta;
	private AccountService accountService;

	/**
	 * Construtor 
	 * @param v {@link ATMController}
	 */
	public ATMController(ATMView v) {
		this.v = v;
		controlarSteps(v, "incial", null);
		this.conta = new Account(123, 321, 100);
	}

	/**
	 * Registra Evento
	 * @param btn {@link JButton}
	 */
	public void registraEventoBotao(JButton btn) {

		btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {

				if (btn.getText().equals("Limpar")) {
					enter(v, "limpar");
				} else if (btn.getText().equals("Enter")) {
					enter(v, "enter");
				} else {
					v.getjTela().setText(v.getjTela().getText() + btn.getText());
				}
			}
		});
	}

	/**
	 * Define a ação com a tecla ENTER
	 * @param v    {@link ATMView}
	 * @param acao Ação
	 */
	private void enter(ATMView v, String acao) {

		String array[]       = v.getjTela().getText().split(":");
		String tela          = array[0];
		String info          = array[1];
		String valorDigitado = array[2];
		
		this.accountService = new ConsultaAccountService();

		if (acao.equals("limpar")) {
			v.getjTela().setText(tela + ":" + info + ":");
		} else {
			
			//TELA DE PIN
			if (tela.equals("1")) {
				conta.setAccountNumber(Integer.parseInt(valorDigitado));
				controlarSteps(v, "pin", null);
			}
			
			//TELA DE OPCOES OU ERRO
			if (tela.equals("2")) {
				conta.setPin(Integer.parseInt(valorDigitado));
				
				if (accountService.validarUsuario(conta)) {
					controlarSteps(v, "opcoes", null);
				} else {
					controlarSteps(v, "erro", null);
				}
			}
			
			//TELA DE OPCOES
			if (tela.equals("3")) {
				
				if(valorDigitado.equals("1")){
					controlarSteps(v, "saldo", accountService.consultarSaldo(conta));
				}
				
				if(valorDigitado.equals("2")){
					controlarSteps(v, "saque", null);
				}
				
				if(valorDigitado.equals("3")){
					controlarSteps(v, "deposito", null);
				}
				
				if(valorDigitado.equals("4")){
					controlarSteps(v, "incial", null);
				}
				
			}
			
			//TELA DE SAQUE
			if (tela.equals("4")) {
				
				Double valor = null;
				
				//Instancia Saque
				accountService = new SaqueAccountService();
				
				if(valorDigitado.equals("1")){
					valor = 20.0;
				}
				
				if(valorDigitado.equals("2")){
					valor = 40.0;		
				}
				
				if(valorDigitado.equals("3")){
					valor = 60.0;
				}
				
				if(valorDigitado.equals("4")){
					valor = 100.0;
				}
				
				if(valorDigitado.equals("5")){
					valor = 200.0;
				}
				
				if(valorDigitado.equals("6")){
					controlarSteps(v, "incial", null);
				}
				
				if(accountService.efetuarSaque(conta, valor)) {
					controlarSteps(v, "saque concluido", null);
				} else {
					controlarSteps(v,"erro de saque",null);
				}
			}
			
			if (tela.equals("5")) {
				
				if(valorDigitado.equals("1")){
					controlarSteps(v, "opcoes", null);
				}
				
				if(valorDigitado.equals("2")){
					controlarSteps(v, "incial", null);	
				}
			}
			
			if (tela.equals("6")) {
				
				//Instancia Saque
				accountService = new DepositoAccountService();
				
				if(valorDigitado.equals("0")){
					controlarSteps(v, "incial", null);	
				}else {
					accountService.depositar(conta, valorDigitado);
					controlarSteps(v, "deposito concluido", null);
				}
			}

		}
	}

	/**
	 * Controla as fases do ATM
	 * @param v    {@link ATMView}
	 * @param tela Tela
	 * @param valor Valor
	 */
	private void controlarSteps(ATMView v, String tela, Double valor) {
		
		if (tela.equals("erro")) {
			v.getjTela().setText("1: Senha ou usuario Invalidos!! \n Seja bem vindo!!!! entre com sua conta:");
		}
		
		if (tela.equals("incial")) {
			v.getjTela().setText("1: Seja bem vindo!!!! entre com sua conta:");
		}
		
		if (tela.equals("pin")) {
			v.getjTela().setText("2: Entre com seu PIN:");
		}
		
		if (tela.equals("opcoes")) {
			v.getjTela().setText(
					  "3: MENU PRINCIPAL        \n"
					+ "    1- Consultar saldo   \n"
					+ "    2- Efetuar Saque     \n"
					+ "    3- Depositar         \n"
					+ "    4- Sair            \n\n"
					+ "Opcao desejada:");
		}
		
		if (tela.equals("saldo")) {
			v.getjTela().setText(
					  "3: SALDO DISPONIVEL = R$ " + valor + "\n\n"
					+ "    MENU PRINCIPAL        \n"
					+ "    1 - Consultar saldo   \n"
					+ "    2 - Efetuar Saque     \n"
					+ "    3 - Depositar         \n"
					+ "    4 - Sair            \n\n"
					+ "Opcao desejada:");
					
		}
		
		if (tela.equals("saque")) {
			v.getjTela().setText(
					  "4: MENU DE SAQUE \n\n"
					+ "	1 - RS 20,00       4 - RS 100,00   \n"
					+ "	2 - RS 40,00       5 - RS 200,00   \n"
					+ "	3 - RS 60,00       6 - Cancelar  \n\n"
					+ "Opcao desejada:");
					
		}
		
		if (tela.equals("erro de saque")) {
			v.getjTela().setText(
					  "3: SALDO INSUFICIENTE!  \n\n"
					+ "    MENU PRINCIPAL        \n"
					+ "    1 - Consultar saldo   \n"
					+ "    2 - Efetuar Saque     \n"
					+ "    3 - Depositar         \n"
					+ "    4 - Sair            \n\n"
					+ "Opcao desejada:");
					
		}
		
		if (tela.equals("saque concluido")) {
			v.getjTela().setText(
					  "5: SAQUE CONCLUIDO COM SUCESSO \n\n"
					+ "OPÇÕES                           \n"
					+ "	1 - Menu principal              \n"
					+ "	2 - Sair                      \n\n"
					+ "Opcao desejada:");
					
		}
		
		if (tela.equals("deposito")) {
			v.getjTela().setText(
					  "6: MENU DEPOSITO \n\n"
					+ "    0 - CANCELAR \n\n"
					+ "Entre com valor desejado:");
					
		}
		
		if (tela.equals("deposito concluido")) {
			v.getjTela().setText(
					  "5: DEPOSITO CONCLUIDO COM SUCESSO \n\n"
					+ "	1 - Menu principal  \n"
					+ "	2 - Sair            \n\n"
					+ "Opcao desejada:");
					
		}
	}
}
