package atm;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Classe de Log do Sistema
 * @author Fittipaldi, Roberval, Túlio, Lucas
 *
 */
public class Log {

	private static final String arqLog = "Log.txt";

	private static Log log;
	
	/**
	 * Construtor
	 */
    private Log() {}
 
    /**
     * Instancia única do objeto
     * @return {@link Log}
     */
    public static synchronized Log getInstance() {
        if (log == null)
        	log = new Log();
 
        return log;
    }

    /**
     * Cria o arquivo Log
     * @param tarefa Tarefa
     */
	public void createLog(String tarefa) {
		try {
			File file = new File(arqLog);

			if (!file.exists() || getNumeroLinhas() >= 1000) {
				file.delete();
				file.createNewFile();
			}

			add(tarefa);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adiciona linha no Arquivo
	 * @param tarefa Tarefa
	 */
	public void add(String tarefa) {

		LocalDateTime agora         = LocalDateTime.now();
		String        dataFormatada = agora.format(DateTimeFormatter.ofPattern("dd/MM/yyyy,HH:mm:ss"));
		String        saida         = dataFormatada + "," + tarefa + ";";
		FileWriter    fw            = null;
		PrintWriter   pw            = null;

		try {
			File arquivo = new File(arqLog);
			
			fw = new FileWriter(arquivo, true);
			pw = new PrintWriter(fw);
			pw.println(saida);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pw.close();
				fw.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	/**
	 * Rertorna o Número de Linhas
	 * @return Número de Linhas
	 * @throws IOException
	 */
	public int getNumeroLinhas() throws IOException {

		LineNumberReader lnr = new LineNumberReader(new FileReader(new File(arqLog)));
		
		lnr.skip(Long.MAX_VALUE);
		int retorno = lnr.getLineNumber();
		lnr.close();

		return retorno;
	}

}
